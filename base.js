module.exports = {
  "env": {
    "browser": true,
    "es6": true,
    "node": true
  },
  "extends": "eslint:recommended",
  "installedESLint": true,
  "parserOptions": {
    "sourceType": "module",
    "allowImportExportEverywhere": false,
    "ecmaVersion": "2017",
    "ecmaFeatures": {
      "experimentalObjectRestSpread": true,
      "classes": true,
      "jsx": true
    }
  },
  "rules": {
    "block-scoped-var": "error",
    "curly": "error",
    "eqeqeq": ["error", "always"],
    "no-case-declarations": "error",
    "no-else-return": "error",
    "no-console" : [
      "error",
      {
        "allow": ["warn", "error"]
      }
    ],
    "no-eval": "error",
    "no-floating-decimal": "warn",
    "no-multiple-empty-lines": [
      "error", 
      {
        "max": 1
      }
    ],
    "indent": ["error", 4],
    "no-multi-spaces": "error",
    "no-process-env": 0,
    "no-redeclare": "error",
    "no-self-assign": "error",
    "no-self-compare": "error",
    "no-unused-vars": [
      "error",
      {
        "vars": "all",
        "args": "after-used"
      }
    ],
    "no-var": "error",
    "prefer-const": "error",
    "array-bracket-spacing": ["error", "never"],
    "object-curly-spacing": ["error", "always"],
    "space-in-parens": ["error", "never"],
    "block-spacing": ["error", "never"],
    "comma-dangle": ["error", "never"],
    "comma-spacing": [
      "error",
      {
        "before": false,
        "after": true
      }
    ],
    "func-style": [
      "error",
      "declaration", {
        "allowArrowFunctions": true
      }
    ],
    "jsx-quotes": ["error", "prefer-double"],
    "key-spacing": [
      "error",
      {
        "afterColon": true
      }
    ],
    "keyword-spacing": [
      "error",
      {
        "before": true
      }
    ],
    "quotes": [
      "error",
      "single"
    ],
    "quote-props": ["error", "consistent-as-needed"],
    "semi": [
      "error",
      "always"
    ]
  }
};
