module.exports = {
  "extends": "burstsms/base",
  "parser": "babel-eslint",
  "plugins": [
    "react"
  ],
  "rules": {
    "react/jsx-uses-react": "error",
    "react/jsx-uses-vars": "error",
		"react/jsx-curly-spacing": ["error", "always"],
		"react/no-is-mounted": "error"
  }
};
